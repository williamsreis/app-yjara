export class Passageiro {
    id: number;
    cpf: string;
    nome: string;
    email: string;
    nascimento: string;
    idade: number;
    telefone: string;
    pontos: number;
}
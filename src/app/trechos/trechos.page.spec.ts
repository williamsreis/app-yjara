import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrechosPage } from './trechos.page';

describe('TrechosPage', () => {
  let component: TrechosPage;
  let fixture: ComponentFixture<TrechosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrechosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrechosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

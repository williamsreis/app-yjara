import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Trecho } from './../models/trecho';
import { PoseidonService } from './../api/poseidon.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-trechos',
  templateUrl: './trechos.page.html',
  styleUrls: ['./trechos.page.scss'],
})
export class TrechosPage implements OnInit {

  params: any = {};
  trechos: Array<Trecho>;

  constructor(
    private servicePoseidon: PoseidonService,
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(params);
      this.params = params;
    });
    this.servicePoseidon.getTrechos().subscribe(trechos => {
      this.trechos = trechos;
      console.log(this.trechos);
   });
  }

  getConfirmacao(trecho) {
    const data = {
      cpf: this.params.cpf,
      nome: this.params.nome,
      email: this.params.email,
      nascimento: this.params.nascimento,
      telefone: this.params.telefone,
      pontos: this.params.pontos,
      convenio: this.params.convenio,
      convenioNome: this.params.convenioNome,
      convenioDesconto: this.params.convenioDesconto,
      viagem: this.params.viagem,
      viagemOrigemDestino: this.params.viagemOrigemDestino,
      trecho: trecho.id,
      trechoOrigemDestino: trecho.origem + '/' + trecho.destino,
      trechoValor: trecho.valor,
      trechoValorPonto: trecho.valor_ponto,
    };
    console.log('data: ', data);
    this.router.navigate(['/forma-pagamento', data]);
  }
  onVoltar() {
    this.location.back();
  }
}

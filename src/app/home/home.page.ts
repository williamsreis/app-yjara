
import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';
import { AuthenticationService } from './../services/authentication.service';
import { Passageiro } from './../models/passageiro';
import { PoseidonService } from './../api/poseidon.service';
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  
  public passageiro: Passageiro = new Passageiro();
  homeForm: FormGroup;
  cpf = '';
  disabledButton: boolean;


  constructor(
    private router: Router,
    private servicePoseidon: PoseidonService,
    public formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private bluetoothSerial: BluetoothSerial,
  ) {
    this.cpf = '';
  }
  ionViewWillEnter() {
    this.disabledButton = false;
    this.cpf = "";
  }
  ngOnInit() {
    this.disabledButton = false;
    
    //this.cpf = this.route.snapshot.paramMap.get('cpf');
    this.homeForm = this.formBuilder.group({
      cpf: new FormControl(this.cpf, Validators.required)
    });
  }
  onSubmit(value) {
    this.disabledButton = true
    this.cpf = '';
    if (value.cpf !== "") {
    this.servicePoseidon.getPassageiro(value.cpf).subscribe(passageiro => {
      this.passageiro = passageiro;
      if (passageiro.id) {
        //this.disabledButton = false;
        this.router.navigate(['/passageiros', {
          nome: this.passageiro.nome,
          cpf: this.passageiro.cpf,
          email: this.passageiro.email,
          nascimento: this.passageiro.nascimento,
          telefone: this.passageiro.telefone,
          pontos: this.passageiro.pontos,
        }]);
      } else {
        //this.disabledButton = false;
        this.router.navigate(['/passageiros', {cpf: value.cpf}]);
      }
    });
    } else {
     // this.disabledButton = false;
      this.router.navigate(['/passageiros', {cpf: value.cpf}]);
    }
    
  }

  onLogout() {
    this.authService.logout();
  }

  onImpressoras() {
    this.router.navigate(['/impressoras']);
  }
  print() {
    const result = '';

   console.log('print');
    this.bluetoothSerial.write(result).then(function(success) {
      console.log(success);
    }, function(error) {
      console.log(error);
    });
  }
}
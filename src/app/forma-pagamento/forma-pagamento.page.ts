import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-forma-pagamento',
  templateUrl: './forma-pagamento.page.html',
  styleUrls: ['./forma-pagamento.page.scss'],
})
export class FormaPagamentoPage implements OnInit {

  formas: String[];
  params: any = {};
  
  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(params);
      this.params = params;
    });
    this.formas = ['Dinheiro','Cartão de Crédito','Cartão de Débito']

    if( (this.params.trechoValorPonto > 0) && (parseInt(this.params.pontos, 10) >= this.params.trechoValorPonto)) {
      console.log(this.params);
      this.formas.push('Programa Fidelidade')
    }
  }

  onVoltar() {
    this.location.back();
  }

  getFormaPagamento(forma: any) {
    
    
    const data = {
      cpf: this.params.cpf,
      nome: this.params.nome,
      email: this.params.email,
      nascimento: this.params.nascimento,
      telefone: this.params.telefone,
      pontos: this.params.pontos,
      convenio: this.params.convenio,
      convenioNome: this.params.convenioNome,
      convenioDesconto: this.params.convenioDesconto,
      viagem: this.params.viagem,
      viagemOrigemDestino: this.params.viagemOrigemDestino,
      trecho: this.params.trecho,
      trechoOrigemDestino: this.params.trechoOrigemDestino,
      trechoValor: this.params.trechoValor,
      trechoValorPonto: this.params.trechoValorPonto,
      formaPagamento: forma
    };

    this.router.navigate(['/confirmacao', data]);
  }

}

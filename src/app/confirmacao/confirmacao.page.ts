import { Storage } from '@ionic/storage';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PoseidonService } from '../api/poseidon.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-confirmacao',
  templateUrl: './confirmacao.page.html',
  styleUrls: ['./confirmacao.page.scss'],
})
export class ConfirmacaoPage implements OnInit {
  params;
  valorTrecho: any;
  valorTrechoPonto: number;
  user;
  message;
  disabledButton: boolean;

  constructor(
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private servicePoseidon: PoseidonService,
    private storage: Storage,
    public toastController: ToastController,
  ) { }
  ionViewWillEnter() {
    this.disabledButton = false;   
  }
  ngOnInit() {

    this.route.params.subscribe(params => {
      console.log(params);
      this.params = params;
      this.valorTrecho = this.params.trechoValor;
      this.valorTrechoPonto = this.params.trechoValorPonto;

      this.valorTrecho = this.valorTrecho - (this.valorTrecho * this.params.convenioDesconto) / 100;
      const idade = this.calcularIdade(this.params.nascimento);
      if (idade < 6 || idade >= 65) {
        this.valorTrecho = 0;
      }
    });
    this.storage.get('user').then(res => {
      if (res) {
        this.user = res;
      }
    });
  }

  onVoltar() {
    this.location.back();
  }
  onCancelar() {
    this.router.navigate(['/home']);
  }

  async onConfirmar() {
    this.disabledButton = true
    const data = {
      cpf: this.params.cpf,
      nome: this.params.nome,
      email: this.params.email,
      nascimento: this.params.nascimento,
      telefone: this.params.telefone,
      pontos: this.params.pontos,
      convenio: this.params.convenio,
      convenioNome: this.params.convenioNome,
      convenioDesconto: this.params.convenioDesconto,
      viagem: this.params.viagem,
      viagemOrigemDestino: this.params.viagemOrigemDestino,
      trecho: this.params.trecho,
      trechoOrigemDestino: this.params.trechoOrigemDestino,
      trechoValor: this.params.trechoValor,
      trechoValorPonto: this.params.trechoValorPonto,
      user: this.user.id,
      embarcacao: this.user.embarcacao_id,
      formaPagamento: this.params.formaPagamento
    };
    await this.servicePoseidon.setPassagem(data)
    .subscribe(res => {
      console.log(res);
      this.presentToastWithOptions(res.message);
      this.router.navigate(['/home']);
    }, (err) => {
      console.log(err);
    });
  }

  calcularIdade(nascimento: Date) {
    const hoje = new Date();
    nascimento = new Date(nascimento);
    let diferencaAnos = hoje.getFullYear() - nascimento.getFullYear();
    const data1 = new Date(hoje.getFullYear(), hoje.getMonth(), hoje.getDate());
    const data2 = new Date(hoje.getFullYear(), nascimento.getMonth(), nascimento.getDate());
    if (data1  <   data2) {
        diferencaAnos = diferencaAnos - 1 ;
    }
    return diferencaAnos;
  }

  async presentToastWithOptions(message) {
    this.message = message;
    const toast = await this.toastController.create({
      message: this.message,
      showCloseButton: true,
      position: 'top',
      duration: 2000,
      closeButtonText: 'Ok',
    });
    toast.present();
  }

}

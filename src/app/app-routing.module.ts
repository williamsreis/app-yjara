import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home',  canActivate: [AuthGuardService], loadChildren: './home/home.module#HomePageModule' },
  { path: 'convenios', canActivate: [AuthGuardService], loadChildren: './convenios/convenios.module#ConveniosPageModule' },
  { path: 'passageiros', canActivate: [AuthGuardService], loadChildren: './passageiros/passageiros.module#PassageirosPageModule' },
  { path: 'viagem', canActivate: [AuthGuardService], loadChildren: './viagem/viagem.module#ViagemPageModule' },
  { path: 'trechos', canActivate: [AuthGuardService], loadChildren: './trechos/trechos.module#TrechosPageModule' },
  { path: 'confirmacao', canActivate: [AuthGuardService], loadChildren: './confirmacao/confirmacao.module#ConfirmacaoPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'impressoras', loadChildren: './impressoras/impressoras.module#ImpressorasPageModule' },
  { path: 'forma-pagamento', loadChildren: './forma-pagamento/forma-pagamento.module#FormaPagamentoPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

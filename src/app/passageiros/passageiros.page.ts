import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Passageiro } from './../models/passageiro';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-passageiros',
  templateUrl: './passageiros.page.html',
  styleUrls: ['./passageiros.page.scss'],
})
export class PassageirosPage implements OnInit {
  
  passageiro: Passageiro = new Passageiro();
  passageiros_form: FormGroup;
  disabledButton: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private location: Location
  ) {
      this.passageiro = new Passageiro();
      this.passageiro.cpf = '';
      this.passageiro.nome = '';
      this.passageiro.email = '';
      this.passageiro.nascimento = '';
      

   }

   ionViewWillEnter() {
    this.disabledButton = false;
  } 

  ngOnInit() {
    this.disabledButton = false;
    
    this.passageiro.cpf = this.route.snapshot.paramMap.get('cpf');
    this.passageiro.nome = this.route.snapshot.paramMap.get('nome');
    this.passageiro.email = this.route.snapshot.paramMap.get('email');
    this.passageiro.nascimento = this.route.snapshot.paramMap.get('nascimento');
    this.passageiro.telefone = this.route.snapshot.paramMap.get('telefone');
    this.passageiro.pontos =  parseInt(this.route.snapshot.paramMap.get('pontos'), 10);

    if(!this.passageiro.pontos){
      this.passageiro.pontos = 0;
    }
    
    
    this.passageiros_form = this.formBuilder.group({
      cpf: new FormControl(this.passageiro.cpf),
      nome: new FormControl(this.passageiro.nome, Validators.required),
      email: new FormControl(this.passageiro.email, [Validators.email]),
      nascimento: new FormControl(this.passageiro.nascimento, [Validators.required]),
      telefone: new FormControl(this.passageiro.telefone, [Validators.required])
    });
    console.log(this.passageiro);
  }

  onCancelar() {
    this.location.back();
  }


  onSubmit(value) {
    this.disabledButton = true;
    value.pontos = this.passageiro.pontos;
    console.log(value);
    this.router.navigate(['/convenios', value]);
  }
}
;
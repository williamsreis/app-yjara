import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassageirosPage } from './passageiros.page';

describe('PassageirosPage', () => {
  let component: PassageirosPage;
  let fixture: ComponentFixture<PassageirosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassageirosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassageirosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Location } from '@angular/common';
import { Passageiro } from './../models/passageiro';
import { Convenio } from './../models/convenio';
import { PoseidonService } from './../api/poseidon.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-convenios',
  templateUrl: './convenios.page.html',
  styleUrls: ['./convenios.page.scss'],
})
export class ConveniosPage implements OnInit {
  convenios: Array<Convenio>;
  passageiro: any;
  conveniosOptions = [];
  constructor(
    private router: Router,
    private servicePoseidon: PoseidonService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.route.params.subscribe(passageiro => {
      this.passageiro = passageiro;
    });
    this.servicePoseidon.getConvenios().subscribe(convenios =>{
      this.convenios = convenios;
      console.log(this.convenios);
    });
  }
  onAvancar(convenio = null) {
    const data = {
      cpf: this.passageiro.cpf,
      nome: this.passageiro.nome,
      email: this.passageiro.email,
      nascimento: this.passageiro.nascimento,
      telefone: this.passageiro.telefone,
      pontos: parseInt(this.passageiro.pontos,10),
      convenio: null,
      convenioNome: 'Nenhum',
      convenioDesconto: 0
    };
    if (convenio) {
      data.convenio = convenio.id;
      data.convenioNome = convenio.nome;
      data.convenioDesconto = convenio.desconto;
    }
    console.log(data);
    this.router.navigate(['/viagem', data]);
  }
  onCancelar() {
    this.location.back();
  }

}

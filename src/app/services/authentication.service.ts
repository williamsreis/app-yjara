import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';

const TOKEN_KEY = 'token';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  authenticationState = new BehaviorSubject(false);

  constructor(
    private storage: Storage,
    private plt: Platform
  ) {
    this.plt.ready().then(() => {
      this.checkToken();
    });
   }

   checkToken() {
    this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.authenticationState.next(true);
      }
    });
  }

    login(resposta,loader) {
      this.storage.set('user', resposta.user).then( () => {
        console.log('user gravado');
        localStorage.setItem('token',resposta.token);
      });
      return this.storage.set(TOKEN_KEY, resposta.token).then(() => {
        this.authenticationState.next(true);
        loader.dismiss();
      });
    }

    logout() {
      this.storage.remove('user').then(() => {
        console.log('user removido');
      });
      return this.storage.remove(TOKEN_KEY).then(() => {
        this.authenticationState.next(false);
      });
    }

    isAuthenticated() {
      return this.authenticationState.value;
    }

    getAuthorizationToken() {
      this.storage.get(TOKEN_KEY).then(res => {
        if (res) {
          return res;
        }
      });
    }
  }
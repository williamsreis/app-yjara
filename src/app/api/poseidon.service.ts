import { Trecho } from './../models/trecho';
import { Convenio } from './../models/convenio';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Passageiro } from '../models/passageiro';
import { Viagem } from '../models/viagem';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class PoseidonService {

  poseidonUrl = 'http://yjara.w3mais.com/api/';
  //poseidonUrl = 'http://yjara.desenv.br/api/';

  constructor(
    private http: HttpClient,
    public alertController: AlertController
  ) {
  }

  login(credenciais: any): Observable<any> {
    console.log('credencias: ', credenciais);
    return this.http.post(this.poseidonUrl + 'login', credenciais)
    .pipe(
      catchError(this.handleError('login', []))
    );
  }
  setPassagem(data: any): Observable<any> {
    return this.http.post(this.poseidonUrl + 'passageiro-viagem', data)
    .pipe(
      catchError(this.handleError('setPassagem', []))
    );
  }
  getConvenios(): Observable<Convenio[]> {
    return this.http.get<Convenio[]>(this.poseidonUrl + 'convenios')
      .pipe(
        catchError(this.handleError('getConvenios', []))
      );
  }

  getViagens(): Observable<Viagem[]> {
    return this.http.get<Viagem[]>(this.poseidonUrl + 'viagens')
      .pipe(
        catchError(this.handleError('getViagens', []))
      );
  }

  getTrechos(): Observable<Trecho[]> {
    return this.http.get<Trecho[]>(this.poseidonUrl + 'trechos')
      .pipe(
        catchError(this.handleError('getViagens', []))
      );
  }


  getPassageiro(cpf: any): Observable<Passageiro> {

    return this.http.get<Passageiro>(this.poseidonUrl + 'passageiros/' + cpf);
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Alerta',
      subHeader: 'Mensagem de Erro',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(operation + ' failed: ' + error.message);
      this.presentAlert(error.message);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Viagem } from './../models/viagem';
import { PoseidonService } from './../api/poseidon.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-viagem',
  templateUrl: './viagem.page.html',
  styleUrls: ['./viagem.page.scss'],
})
export class ViagemPage implements OnInit {

  viagens: Array<Viagem>;
  params;
  constructor(
    private servicePoseidon: PoseidonService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(params);
      this.params = params;
    });
    this.servicePoseidon.getViagens().subscribe(viagens => {
      this.viagens = viagens;
      console.log(this.viagens);
   });
  }

  getTrecho(viagem) {
    
    const data = {
      cpf: this.params.cpf,
      nome: this.params.nome,
      email: this.params.email,
      nascimento: this.params.nascimento,
      telefone: this.params.telefone,
      pontos: this.params.pontos,
      convenio: this.params.convenio,
      convenioNome: this.params.convenioNome,
      convenioDesconto: this.params.convenioDesconto,
      viagem: viagem.id,
      viagemOrigemDestino: viagem.origem + ' - ' + viagem.destino
    };
    this.router.navigate(['/trechos', data]);
  }
  onVoltar() {
    this.location.back();
  }

}
